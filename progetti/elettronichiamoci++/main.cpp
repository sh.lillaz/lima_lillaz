#include <iostream>

using namespace std;

int main() {
    
    double R1, R2, V;

    
    cout << "Inserisci il valore di R1 : ";
    cin >> R1;

    cout << "Inserisci il valore di R2 : ";
    cin >> R2;

    cout << "Inserisci il valore di V : ";
    cin >> V;

    
    double I_totale = V / (R1 + R2);

    
    double V1 = I_totale * R1;
    double V2 = I_totale * R2;

    
    cout << "La corrente totale è: " << I_totale << " ampere" <<endl;
    cout << "La tensione attraverso R1 è: " << V1 << " volt" <<endl;
    cout << "La tensione attraverso R2 è: " << V2 << " volt" <<endl;

    return 0;
}
