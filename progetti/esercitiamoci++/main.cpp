#include <iostream>

using namespace std;

int main() {
    int num;

    cout << "Inserisci un numero: ";
    cin >> num;

    cout << "Divisori di " << num << ": ";

    for (int I = 1; I <= num; i++) {
        if (num % I == 0) {
            cout << I << " ";
        }
    }

    return 0;
}