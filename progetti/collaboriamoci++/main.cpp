//stampa il contenuto di un un vettore v[] di n elementi per riga senza altro testo.
#include <iostream>

int main() {
  int n = 10; // Numero di elementi nel vettore
  int v[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}; // Vettore di esempio

  for (int i = 0; i < n; i++) {
    cout << v[i] << " ";
    if ((i + 1) % n == 0) {
      cout << endl;
    }
  }

  return 0;
}
//riempi un vettore v[] di n elementi in modo casuale con numeri "da""a".
for(int i = 0; i < n; i++) {
        v[i] = rand() % (b - a + 1) + a;
    }









































#include <iostream>
#include <cstdlib>
#include <ctime>

int main() {
    int n;
    int a, b;
    
    // Leggi il numero di elementi del vettore
    cout << "Inserisci il numero di elementi del vettore: ";
    cin >> n;
    
    // Leggi l'intervallo di numeri casuali
    cout << "Inserisci gli estremi dell'intervallo [a, b]: ";
    cin >> a >> b;
    
    // Inizializza il generatore di numeri casuali
    srand(time(null));
    
    // Crea il vettore di dimensione n
    int v[n];
    
    // Riempie il vettore con numeri casuali compresi tra a e b
    for(int i = 0; i < n; i++) {
        v[i] = rand() % (b - a + 1) + a;
    }
    
    // Stampa il vettore
    cout << "Vettore generato: ";
    for(int i = 0; i < n; i++) {
        cout << v[i] << " ";
    }
    cout << endl;
    
    return 0;
}