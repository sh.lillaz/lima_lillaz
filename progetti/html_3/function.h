//function.h
#ifndef FUNCTION_H
#define FUNCTION_H
#define N 5 
#include <iostream>
#include <fstream>
using namespace std;
int casualeTra(int min,int max);
void help(int argc, char** argv);
void scambia(int &n1,int &n2);
bool esiste(string nome);
class pagina_html{
    public :
        pagina_html();
        pagina_html(string titolo);
        void setTitolo(string titolo);
        void setNomeFile(string nomeFile);
        void addBody(string elemento);
        void addHead(string elemento);
        void salva();
        void creaHead();
        void creaBody();
        string getHead();
        string getBody();
        string getPagina();    
        string getTitolo();
        string getNomeFile();
        string getLingua();
    private :
    string nomeFile;
    string titolo;
    string body;
    string head;
};


#endif 
