markdown.md
```mermaid
flowchart TD
    A[Inizio] --> B[Chiedi un numero]
    B --> C{Il numero è pari?}
    C --> |Si| D[Stampa Il numero è pari]
    C --> |No| E{Il numero è multiplo di 5?}
    D --> E
    E --> |Si| F[Hai inserisco un multiplo di 5]
    F --> G[Fine]
    E --> |No|B
```