//34.a 1 eserczio
#include <iostream>

using namespace std;

int main(int argc, char* argv[]) {
    cout << "Numero di argomenti passati: " << argc << endl;
    
    for(int i = 0; i < argc; i++) {
        cout << "Argomento " << i << ": " << argv[i] << endl;
    }

    return 0;
}