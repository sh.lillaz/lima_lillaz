#include "function.h"

int main() {
    vector<Alunno> alunni = {
        {"Mario", "Rossi", {
            {"Matematica", "2025-01-10", 8, {{"Cosa sono le equazioni?"}, {"Definizione di funzione"}}},
            {"Italiano", "2025-01-12", 7, {{"Analisi di una poesia"}, {"Temi del Decadentismo"}}}
        }},
        {"Luigi", "Bianchi", {
            {"Storia", "2025-01-11", 6, {{"Cause della Prima Guerra Mondiale"}}},
            {"Fisica", "2025-01-13", 9, {{"Leggi di Newton"}}}
        }},
        {"Giulia", "Verdi", {
            {"Biologia", "2025-01-14", 10, {{"Struttura del DNA"}}},
            {"Chimica", "2025-01-15", 8, {{"La tavola periodica"}}}
        }}
    };

    generaHTML(alunni);
    return 0;
}