#ifndef FUNCTION_H
#define FUNCTION_H

#include <string>
#include <vector>

using namespace std;

struct Domanda {
    string testo;
};

struct Interrogazione {
    string materia;
    string data;
    int voto;
    vector<Domanda> domande;
};

struct Alunno {
    string nome;
    string cognome;
    vector<Interrogazione> interrogazioni;
};

void generaHTML(const vector<Alunno>& alunni);

#endif