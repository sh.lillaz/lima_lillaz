#include "function.h"
#include <fstream>
#include <iostream>

void generaHTML(const vector<Alunno>& alunni) {
    ofstream file("elenco_alunni.html");

    if (file.is_open()) {
        file << "<!DOCTYPE html>\n";
        file << "<html lang=\"it\">\n";
        file << "<head>\n";
        file << "    <meta charset=\"UTF-8\">\n";
        file << "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n";
        file << "    <title>Elenco Alunni</title>\n";
        file << "</head>\n";
        file << "<body>\n";
        file << "    <h1>Elenco Alunni</h1>\n";

        for (const auto& alunno : alunni) {
            file << "    <h2>" << alunno.nome << " " << alunno.cognome << "</h2>\n";

            for (const auto& interrogazione : alunno.interrogazioni) {
                file << "    <h3>Materia: " << interrogazione.materia
                     << " (Data: " << interrogazione.data
                     << ", Voto: " << interrogazione.voto << ")</h3>\n";
                file << "    <ul>\n";

                for (const auto& domanda : interrogazione.domande) {
                    file << "        <li>" << domanda.testo << "</li>\n";
                }

                file << "    </ul>\n";
            }
        }

        file << "</body>\n";
        file << "</html>\n";

        file.close();
        cout << "File HTML generato con successo: elenco_alunni.html" << endl;
    } else {
        cout << "Errore nell'apertura del file HTML." << endl;
    }
}